var userAgent = navigator.userAgent.toLowerCase();

var isIE = window.ActiveXObject && userAgent.indexOf('msie') != -1 && userAgent.substr(userAgent.indexOf('msie') + 5, 3);

function $$(id) {return typeof id == "string" ? document.getElementById(id) : id;}
//将已转义的字符恢复
function html(str) {
	 return str.replace(/\\/g, '').replace(/\&amp;/g, '&').replace(/\&#039;/g, "'").replace(/\&quot;/g, '"').replace(/\&lt;/g, '<').replace(/\&gt;/g, '>');
}

//新消息闪动页面标题
function flashTitle() {
	clearInterval(tttt);
	flashtitle_step=1;
	flashtitle_step_a = 0;
	tttt = setInterval(function(){
		flashtitle_step_a++;
		if(flashtitle_step_a>=15){
			sounder.html(welive.sound1);
			flashtitle_step_a = 0;
		}
		if (flashtitle_step==1) {
			document.title='【新消息】'+pagetitle;
			flashtitle_step=2;
		}else{
			document.title='【      】'+pagetitle;
			flashtitle_step=1;
		}
	}, 800);
}

//停止闪动页面标题
function stopFlashTitle() {
	if(flashtitle_step != 0){
		flashtitle_step=0;
		clearInterval(tttt);
		document.title=pagetitle;
	}
}

//获得计算机当前时间
function getLocalTime() {
	var date = new Date();

	function addZeros(value, len) {
		var i;
		value = "" + value;
		if (value.length < len) {
			for (i=0; i<(len-value.length); i++)
				value = "0" + value;
		}
		return value;
	}
	return addZeros(date.getHours(), 2) + ':' + addZeros(date.getMinutes(), 2) + ':' + addZeros(date.getSeconds(), 2);
}

//显示提示信息 callback表示对话框关闭时执行的函数; success表示是成功信息还是错误信息; time是自动关闭时间(秒)
function showInfo(info, title, callback, time, success){
	var ti = time? time * 1000 : 0;

	if(success){
		var title = "<font color=#33CC00>" + (title? title : "系统信息") + "</font>";
		var content = "<font color=blue>" + info + "</font>";
	}else{
		var title = "<font color=red>" + (title? title : "系统信息") + "</font>";
		var content = "<font color=#FF9900>" + info + "</font>";
	}

	easyDialog.open({
		container:{
			header: title,
			content: content,
			yesFn:function(){},
			yesText: '确定'
		},
		autoClose:ti,
		callback: callback
	});

	$("#easyDialogYesBtn").focus(); //确定按钮获得焦点
}

//显示确认操作对话框 callback表示按确定时执行的函数; time是自动关闭时间;
function showDialog(info, title, callback, time){
	var ti = time? time * 1000 : 0;

	easyDialog.open({
		container:{
			header: "<font color=red>" + (title? title : "系统信息") + "</font>",
			content: "<font color=#FF9900>" + info + "</font>",
			yesFn: callback,
			yesText: '确定',
			noFn:true,
			noText: '取消'
		},
		autoClose:ti
	});

	$("#easyDialogYesBtn").focus(); //确定按钮获得焦点
}

//顶部下拉菜单 b为参数对象, c为下拉菜单显示后的事件函数
(function(a) {
	a.fn.Jdropdown = function(b, c) {
		if (this.length) {
			"function" == typeof b && (c = b, b = {});
			var d = a.extend({
					event: "mouseover",
					current: "hover",
					delay: 0
				}, b || {}),
				e = "mouseover" == d.event ? "mouseout" : "mouseleave";
			a.each(this, function() {
				var b = null,f = null,g = !1;
				a(this).bind(d.event, function() {
					if (g) clearTimeout(f);
					else {
						var e = a(this);
						b = setTimeout(function() {
							e.addClass(d.current), g = !0, c && c(e)
						}, d.delay);
					}
				}).bind(e, function() {
					if (g) {
						var c = a(this);
						f = setTimeout(function() {
							c.removeClass(d.current), g = !1
						}, 0)
					} else clearTimeout(b);
				});
			});
		}
	}
})(jQuery);

//创建窗口
function createWin(id, title, recs, lang, au){
	var x = x_win_content.replace('888888', recs);  // recs聊天内容
	Create(id, title, x, lang, au);
}

//打开窗口
function openWin(id){ Show(id);}

//关闭窗口
function closeWin(id) { 
	Min(id);
	window.event?window.event.cancelBubble=true:event.stopPropagation();
}
  // 创建窗口元素
function Create(id, title, wbody, lang, au){ 
	var winbody = document.getElementById('x-bodymid');
	winbody.childNodes[1].innerHTML += wbody;  // 创建聊天框
	winbody.childNodes[1].lastChild.setAttribute("id", "wicket" + id);
	winbody.childNodes[1].lastChild.children[0].setAttribute("id","overview" + id);
	winbody.childNodes[1].lastChild.style.display = "none";  // 默认进来是隐藏的
	var overviewCount = winbody.childNodes[1].children.length;
	if(overviewCount === 1) {// 只有一个访客时进来必须是显示
		openWin(id,au);
		winbody.childNodes[1].lastChild.style.display = "block";
		$('#g'+ id).addClass("active-guest");
	}
	var g_tools  = $(winbody).children(".g_tools"); 
	//客人有上传授权
	if(au && auth_upload){
		g_tools.children(".t_offupload").show();
	}else if(auth_upload){
		g_tools.children(".t_authupload").show();
	}
};

// 显示窗口
function Show(id){
	welive.where = 1; //1客人区   0群聊区
	s_title.addClass("off");
	var xwin = document.getElementsByClassName('mywin')[0]; 
	var myRecord = document.getElementsByClassName('recordContent')[0];  // 客人信息框
	xwin.id = "win"+id; //  设置当前窗口id
	myRecord.setAttribute("id", "myRecord"+ id);   // 设置记录客人信息框的id      
	var o = $('#wicket' + id);
	var g_tools  = $("#x-bodymid").children(".g_tools"); //获取id
	g_tools.children(".t_smilies").tipTip({content: $(".smilies_div").html().replace(/towhere/ig, id), keepAlive:true, maxWidth:"242px", defaultPosition:"top", edgeOffset:0, delay:300, parent: true, hoverClass: "hover"});
	g_tools.children(".t_authupload").tipTip({content: '<input class="save" type="submit" value="授权上传图片" onclick="guest_authupload();return false;">', activation:"click", keepAlive:true, maxWidth:"348px", defaultPosition:"top", edgeOffset:4, delay:200, hoverClass: "hover"});
	g_tools.children(".t_offupload").tipTip({content: '<input class="save" type="submit" value="解除上传授权" onclick="guest_offupload();return false;">', activation:"click", keepAlive:true, maxWidth:"348px", defaultPosition:"top", edgeOffset:4, delay:200, hoverClass: "hover"});
	g_tools.children(".t_kickout").tipTip({content: '<input class="save" type="submit" value="确定踢出" onclick="guest_kickout();return false;">', activation:"click", keepAlive:true, maxWidth:"348px", defaultPosition:"top", edgeOffset:4, delay:200, hoverClass: "hover"});
	g_tools.children(".t_banned").tipTip({content: '<input class="save" type="submit" value="确定禁言" onclick="guest_banned();return false;">', activation:"click", keepAlive:true, maxWidth:"348px", defaultPosition:"top", edgeOffset:4, delay:200, hoverClass: "hover"});
	g_tools.children(".t_transfer").tipTip({content: '<div class="s_transfer">&nbsp;</div>', enter: function(){get_supporters();}, activation:"click", keepAlive:true, maxWidth:"348px", defaultPosition:"top", edgeOffset:0, delay:0, parent: true, hoverClass: "hover"});
	if(id != CurrentId){
		o.css({'display':'block','visibility':'visible'}).siblings().css({'display':'none','visibility':'hidden'}); 
		g_online.find("#g" + id).addClass('active-guest').siblings().removeClass("active-guest");
		g_online.find("#g" + id + ">b").html(0).hide(); //隐藏未读信息数  
		CurrentId = id;
		o.parent().parent().find('#text_in').select();
	}
	if(guest.length > 0){
		$('.bt').children().css("visibility","visible");
	}
	get_guestInfo(CurrentId);
	scrollbar(CurrentId)//更新滚动条
	o.find(".overview>div.updating").remove(); //删除输入状态
};

// 更新滚动条
function scrollbar(id){
	var scrollbar = $('#overview' + id);
		if (id == '0') return; 
	scrollbar.scrollTop (scrollbar[0].scrollHeight); //更新滚动条 
}

// 关闭小窗口
function Min(id){   
	if($.inArray(id, offline) > -1) {//如客人离线, 删除所有信息
		guest_delete(id);
		if(guest.length == 0 ){  // 清空客人信息
			var l_note = $(".l_note");
			var g_tools = $(".g_tools");
			g_tools.find(".t_smilies").unbind('mouseenter').unbind('mouseleave');
			l_note.find(".fromurl").html("");
			l_note.find(".ipzone").html("");
			l_note.find("input[name=grade]").removeAttr("checked");
			l_note.find(".chatting-records").children("a").html("");
			l_note.find("input[name=fullname]").val(html(""));
			l_note.find("input[name=phone]").val(html(""));
			l_note.find("input[name=email]").val(html(""));
			l_note.find("input[name=address]").val(html(""));
			l_note.find("textarea[name=remark]").val(html(""));
			$('.bt').children("input").css("visibility","hidden");
		}	
	}
	showLast(id); //显示最上一个小窗口
};

//找到在最上面一个窗口id, 切换为当前
function showLast(id) {
	if(CurrentId !== id) return;// 不是当前离线客人窗口不跳转
	var o,xId = 0;
	$.each(guest, function(i, gid){
		o = $('#wicket' + gid);
        if(o.length && o.css("display") == 'none'){
			xId = gid; 
		}
	});
	if(xId){
		Show(xId);
	}else{
		s_msg.focus(); //如果没有小窗口, 切换到群聊区
	}
}

//websocket
var WebSocket = window.WebSocket || window.MozWebSocket;
if(!WebSocket) {
	var ws_obj = {};
	var ws_ready = false;
	$(window).load(function(){ws_ready = true;});
	$(function(){
		$.ajax({
			url: SYSDIR + 'public/jquery.swfobject.js',
			dataType: 'script',
			async: false,
			cache: true
		});
		
		window.WebSocket = function( a ) {
			a = a.match( /wss{0,1}\:\/\/([0-9a-z_.-]+)(?:\:(\d+)){0,1}/i );
			this.host = a[1];
			this.port = a[2] || 843;
			this.onopen = function(){};
			this.onclose = function(){};
			this.onmessage = function(){};
			this.onerror = function(){};
			this.ready = function(b){return true;};
			this.send = function(b){return ws_obj.call.Send(b);};
			this.close = function(){return ws_obj.call.Close();};
			this.ping = function(){return ws_obj.call.Ping();};
			this.connect = function(){
				ws_obj.call = $('#flash_websocket')[0];
				ws_obj.call.Connect(this.host,this.port );
			};
			this.onmessage_escape = function( d ){
				this.onmessage( {data:unescape( d )} );
			};
			if($('#websocket1212').size()) {
				this.connect();
			}else{
				var div = $('<div></div>').attr({id:'websocket1212'}).css({position:'absolute', top:-999, left:-999});
				div.flash({
					swf: SYSDIR + 'public/websocket.swf?r=' + Math.random(),
					wmode: "window",
					scale: "showall",
					allowFullscreen : true,
					allowScriptAccess : 'always',
					id: 'flash_websocket',
					width : 1,
					height : 1,
					flashvars : {call: 'ws_obj._this'}
				});
				$('body').append(div);
			}
			ws_obj._this = this;
		}
	});
}

//格式化输出信息
function format_output(data) {
	//生成URL链接
	data = data.replace(/((href=\"|\')?(((https?|ftp):\/\/)|www\.)([\w\-]+\.)+[\w\.\/=\?%\-&~\':+!#;]*)/ig, function($1){return getURL($1);});
	
	//生成Email链接
	data = data.replace(/([\-\.\w]+@[\.\-\w]+(\.\w+)+)/ig, '<a href="mailto:$1" target="_blank">$1</a>');
	//将表情代码换成图标路径
	data = data.replace(/\[:(\d*):\]/g, '<img src="' + SYSDIR + 'public/smilies/$1.png">').replace(/\\/g, '');
	return data;
}

//格式化生成URL
function getURL(url, limit) {
	if(url.substr(0, 5).toLowerCase() == 'href=') return url; //实际链接不改变

	if(!limit) limit = 60;
	var urllink = '<a href="' + (url.substr(0, 4).toLowerCase() == 'www.' ? 'http://' + url : url) + '" target="_blank" title="' + url + '">';
	if(url.length > limit) {
		url = url.substr(0, 30) + ' ... ' + url.substr(url.length - 18);
	}
	urllink += url + '</a>';
	return urllink;
}

//插入表情符号
function insertSmilie(code, towhere) {
	code = '[:' + code + ':]';
	if(towhere){
		openWin(towhere);
		var obj = $("#win" + towhere).find(".g_msg")[0];
		if(!obj) return;
	}else{
		var obj = s_msg[0];
	}
	var selection = document.selection;
	obj.focus();

	if(typeof obj.selectionStart != 'undefined') {
		var opn = obj.selectionStart + 0;
		obj.value = obj.value.substr(0, obj.selectionStart) + code + obj.value.substr(obj.selectionEnd);
	} else if(selection && selection.createRange) {
		var sel = selection.createRange();
		sel.text = code;
		sel.moveStart('character', -code.length);
	} else {
		obj.value += code;
	}
}

//插入常用短语
function insertPhrase(me, towhere) {
	var code = $(me).children("b").html();
	$(function() {
		(function($) {
			$.fn.extend({
		  		insertContent : function(myValue, t) {
		  		var $t = $(this)[0];
				if (document.selection) { // ie
		   				this.focus();
		   			var sel = document.selection.createRange();
		   				sel.text = myValue;
		   				this.focus();
		   			sel.moveStart('character', -l);
		    		var wee = sel.text.length;
		   			if (arguments.length == 2) {
		    		var l = $t.value.length;
		    		sel.moveEnd("character", wee + t);
		    		t <= 0 ? sel.moveStart("character", wee - 2 * t - myValue.length) : sel.moveStart( "character", wee - t - myValue.length);
		   				sel.select();
		    		}
		 		} else if ($t.selectionStart|| $t.selectionStart == '0') {
		    	var startPos = $t.selectionStart;
		    	var endPos = $t.selectionEnd;
		    	var scrollTop = $t.scrollTop;
		   			$t.value = $t.value.substring(0, startPos)+ myValue+ $t.value.substring(endPos,$t.value.length);
		    			this.focus();
					$t.selectionStart = startPos + myValue.length;
					$t.selectionEnd = startPos + myValue.length;
					$t.scrollTop = scrollTop;
		    		if (arguments.length == 2) {
						$t.setSelectionRange(startPos - t,
						$t.selectionEnd + t);
		   					this.focus();
		    		}
				} else {
		   			this.value += myValue; 
		   			this.focus();
		            }
		   		}
			})
		})(jQuery);
	});
	$("#text_in").insertContent(code); 
}

//socket连接
function welive_link(){
	welive.ws = new WebSocket(WS_HEAD + document.domain + ':'+ WS_PORT);
	welive.ws.onopen = function(){setTimeout(welive_verify, 100);}; //连接成功后, 小延时再验证用户, 否则IE下刷新时发送数据失败
	welive.ws.onclose = function(){welive_close();};
	welive.ws.onmessage = function(get){welive_parseOut(get.data);};
}

//闪烁标题和声音
function TitleSound(x){
	if(welive.flashTitle && x){
		flashTitle();
		if(welive.sound) sounder.html(welive.sound1);
		welive.flashTitle = 0;
	}else if(welive.flashTitle){
		//flashTitle(); //群聊不闪标题
		if(welive.sound) sounder.html(welive.sound2);
		welive.flashTitle = 0;
	}
}

//清空过长客服对话记录(保留50条)
function welive_clear(){
	var rec = s_history.children("div");
	var len = rec.length;
	if(len >= 100){
		rec.slice(0, len - 50).remove();
		s_hwrap.welivebar_update('bottom');
	}
}

//更新客服数量(在原数量上加或减n)
function admins_update(n){
	var x = parseInt(s_admins.html());
	x = x + n;
	if(x < 0) x = 0;
	s_admins.html(x);
}

//解析数据并输出
function welive_parseOut(data){
	var gid = 0, d = false, type = 0, data = $.parseJSON(data);
	switch(data.x){
		case 4:  //客人实时输入状态
			switch(data.a){
				case 1:
					welive_runtime(data.g, data.i);
					break;

				case 2: //删除当前的输入状态
					$("#win" + data.g).find(".overview>div.updating").remove(); //删除输入状态

					break;
			}

			return;
			break;

		case 1: //客服对话
			var time = getLocalTime();
			if(data.aid == admin.id){ //自己的发言
				d = '<div class=me><u>' + admin.fullname + ' - ' + admin.post + '</u><i>' + time + '</i><br>' + format_output(data.i) + '</div>';
			}else{
				welive.flashTitle = 1;
				d = '<div' + (data.t==1? ' class=a' : '') + '><u>' + data.n + ' - ' + data.p + '</u><i>' + time + '</i><br>' + format_output(data.i) + '</div>';
			}
			break;

		case 2: //客服特别操作及反馈信息
			switch(data.a){
				case 1: //上线
					d = '<div class=i><b></b>' + data.n + ' 上线了</div>';
					
					var status = '';
					welive.flashTitle = 1;

					if(data.fr == 1) status = '<u></u>';

					s_online.append('<li id="index_' + data.ix + '" title="' + data.p + '"><div><img src="' + SYSDIR + 'avatar/' + data.av + '" title="服务中...">' + status + '</div><i' + (data.t==1? ' class=a' : '') + '>' + data.n + '</i></li>');
					s_owrap.welivebar_update(); //更新滚动条
					admins_update(1);
					break;

				case 2: //离线
					d = '<div class=i><b></b>' + data.i + ' 已离线</div>';
					s_online.find("#index_" + data.ix).remove();
					s_owrap.welivebar_update(); //更新滚动条
					admins_update(-1);
					break;

				case 3: //挂起
					welive.flashTitle = 1;
					var a = s_online.find("#index_" + data.ix);
					d = '<div class=i><b></b>' + a.children("i").html() + ' 已挂起</div>';
					a.children("div").append('<b></b>');
					a.children("div").children("img").attr('title', '已挂起');

					if(data.ix == welive.index){
						$(".set_busy").hide();
						$(".set_serving").show();
					}
					break;

				case 4: //解除挂起
					var a = s_online.find("#index_" + data.ix);
					d = '<div class=i><b></b>' + a.children("i").html() + ' 解除挂起</div>';
					a.children("div").children("b").remove();
					a.children("div").children("img").attr('title', '服务中...');

					if(data.ix == welive.index){
						$(".set_serving").hide();
						$(".set_busy").show();
					}
					break;

				case 5: //获取客人信息
					gid = data.g;
					if(typeof data.d == "object"){ //返回的客人数据存在时
						var l_note = $("#myRecord" + gid).find(".l_note");
						if(l_note.length){
							l_note.find("a.fromurl").attr("href", data.d.fromurl.replace(/\&amp;/g, '&')).html(data.d.fromurl);
							l_note.find("a.fromurl").attr("title", data.d.fromurl);
							l_note.find(".ipzone").attr("title",data.d.ipzone + " ( " + data.d.lastip + " )").html(data.d.ipzone + " ( " + data.d.lastip + " )");
							l_note.find(".chatting-records").children("a").html("查看聊天记录");
							l_note.find("input[name=grade][value=" + data.d.grade + "]").prop("checked", "checked").siblings().removeAttr("checked");
							l_note.find("input[name=fullname]").val(html(data.d.fullname));
							l_note.find("input[name=phone]").val(html(data.d.phone));
							l_note.find("input[name=email]").val(html(data.d.email));
							l_note.find("input[name=address]").val(html(data.d.address));
							l_note.find("textarea[name=remark]").val(html(data.d.remark));
							l_note.attr("loaded", 1); //设置数据已更新
						}
					}

					break;

				case 6: //保存客人信息后返回的结果
					gid = data.g; type = 3; d = '此客人信息保存成功!';
					if(data.n != ''){ //客人有姓名时, 更新之
						g_online.children("#g" + gid).attr("title", data.n).children("i").html(data.n);
					}

					break;

				case 7: //重复连接返回的指令
					welive.status = 0;
					welive.autolink = 0;

					clearInterval(ttt_1); //停止发送心跳数据
					clearTimeout(welive.ttt); //停止重连

					s_online.html(""); //清空客服在线列表
					s_admins.html(0); //将客服在线人数清0

					g_online.html(""); //清空客人在线列表
					$("#wicket" + gid).remove(); //清除所有客人小窗口

					$("#websocket1212").remove(); //如果不清除, IE下有冲突

					welive_output('<div class="i"><b></b><font color=red>当前客服已在其它页面登录, 此页面已废弃!!</font></div>');

					break;

				case 8: //客服连接验证成功
					d = '<div class=i><b></b>服务器连接成功!</div>';
					welive.index = data.ix; //socket连接索引值
					s_hwrap.removeClass('loading3');
					welive.status = 1;
					welive.flashTitle = 1;
					//更新自己的客服列表
					var num = 0, status;
					welive.is_robot = parseInt(data.irb); //记录无人值守是否开启
					if(welive.is_robot){
						num += 1;
						s_online.prepend('<li id="index_robot" title="' + data.rp + '"><div><img src="' + SYSDIR + 'avatar/robot/0.png"  title="客服系统正由机器人自动服务中..."></div><i class=a>' + data.rn + '</i></li>');
					}
					$.each(data.al, function(n, a){
						num += 1;
						if(a.b == 1){
							status = 'title="已挂起"><b></b>';
						}else{
							status = 'title="服务中...">';
						}
 						if(a.fr == 1) status += '<u></u>';

						if(a.id == admin.id) admin.aix = a.ix; //记录自己的连接索引值

						s_online.append('<li id="index_' + a.ix + '" title="' + a.p + '"><div><img src="' + SYSDIR + 'avatar/' + a.av + '" ' + status + '</div><i' + (a.t==1? ' class=a' : '') + '>' + a.n + '</i></li>');
					});

					s_owrap.welivebar_update(); //更新滚动条
					admins_update(num); //更新客服人数

					//更新客人的数据
					if(guest.length){ //特殊原因断线后上线(重连)

						$.each(guest, function(i, gid){
							if($.inArray(gid, data.gl) < 0 && $.inArray(gid, offline) < 0) { //客人离线 且不在离线数组中
								offline.push(gid); //添加到离线客人数组中
								guest_output('此客人已离线!', gid, 4); //给客人小窗口输出离线信息

								g_online.find('#g' + gid).addClass("offline");
							}

						});

					}else{ //客服新进入或刷新时
						$.each(data.gl, function(i, guest){
							guest_create(guest.g, guest.n, guest.l, 1, parseInt(guest.au), parseInt(guest.mb)); //创建已连接的客人, 在创建中输出信息, 参数1仅时表示输出特定的信息
						});
					}

					$(".set_busy").click(function(e) {
						showDialog('确定挂起吗?<br>注: 挂起后, 将不再接受新客人加入.<br>但是, 转接过来的客人仍会加入.', '', function(){
							welive_send('x=2&a=3'); //发送挂起请求
						});

						e.preventDefault();
					});

					$(".set_serving").click(function(e) {
						welive_send('x=2&a=4'); //发送解除挂起请求
						e.preventDefault();
					});


					//管理员专有
					if(admin.type == 1){

						//重启socket服务
						$(".reset_socket").click(function(e) {
							showDialog('确定重启Socket通讯服务吗?<br>注: 所有连接将中断! 无特殊原因, 请勿重启.', '', function(){
								clearInterval(ttt_1); //停止发送心跳数据

								welive_send('x=2&a=9'); //发送请求停止socket服务

								setTimeout(function(){
									$.ajax({url: './index.php?c=opensocket&a=ajax', dataType: 'json', async: true, cache: false}); //打开socket服务
								}, 1997); //2秒后重新加载socket服务
							});

							e.preventDefault();
						});

						//开启无人值守
						$(".set_robot_on").click(function(e) {
							showDialog('确定开启无人值守吗?<br>注: 开启无人值守后, 新进入的访客将由机器人自动回复.', '', function(){
								welive_send('x=2&a=10'); //发送开启无人值守请求
							});

							e.preventDefault();
						});

						$(".set_robot_off").click(function(e) {
							showDialog('确定关闭无人值守吗?<br>注: 关闭无人值守后, 新进入的访客将由客服提供服务.', '', function(){
								welive_send('x=2&a=11'); //发送关闭无人值守请求
							});

							e.preventDefault();
						});


						//判断当前无人值守状态及按钮状态
						if(welive.is_robot){
							$(".set_robot_on").hide();
							$(".set_robot_off").show();
						}else{
							$(".set_robot_on").show();
							$(".set_robot_off").hide();
						}
					}


					//启动心跳, 即每隔25秒自动发送一个特殊信息, 解决IE下30秒自动断线的问题
					ttt_1 = setInterval(function() {

						//只要连接状态, 均要发送心跳数据, 设置一个怪异的数字避免与自动离线的时间间隔重合, 避免在同一时间点上send数据上可能产生 -----幽灵bug
						welive.ws.send('x=9&i=1');

					}, 25357);


					break;

				case 10: //开启无人值守时
					welive.is_robot = parseInt(data.irb);
					welive.flashTitle = 1;

					//开启失败
					if(welive.is_robot == 2){
						welive.is_robot = 0;
						showInfo("请先在后台管理->智能->机器人客服管理中添加自动回复内容!", "开启无人值守失败", "", 8, 0);
						return false;
					}

					var a = s_online.find("#index_" + data.ix);
					d = '<div class="i g"><b></b>' + a.children("i").html() + ' 已开启无人值守状态</div>';

					s_online.prepend('<li id="index_robot" title="' + data.p + '"><div><img src="' + SYSDIR + 'avatar/robot/0.png"  title="客服系统正由机器人自动服务中..."></div><i class=a>' + data.n + '</i></li>');
					s_owrap.welivebar_update(); //更新滚动条

					//所有管理员均可设置无人值守
					if(admin.type == 1){
						$(".set_robot_on").hide();
						$(".set_robot_off").show();
					}

					admins_update(1);

					break;

				case 11: //关闭无人值守时

					welive.is_robot = parseInt(data.irb);
					welive.flashTitle = 1;

					var a = s_online.find("#index_" + data.ix);
					d = '<div class="i a"><b></b>' + a.children("i").html() + ' 已关闭无人值守状态</div>';

					s_online.find("#index_robot").remove();
					s_owrap.welivebar_update(); //更新滚动条

					//所有管理员均可设置无人值守
					if(admin.type == 1){
						$(".set_robot_on").show();
						$(".set_robot_off").hide();
					}

					admins_update(-1);

					break;

			}

			break;

		case 5:  //客人与客服对话
			gid = data.g;
			d = data.i;
			if(data.a == 1){
				type = 1; //自己发出的对话
			}else{
				welive.flashTitle = 1; //声音等
				type = 2; //客人发来的
			}

			break;

		case 6: //客服特别操作及反馈信息
			switch(data.a){
				case 8: //客人登录成功
					guest_create(data.g, data.n, data.l, data.re, parseInt(data.au), parseInt(data.mb), data.lastip, data.ipzone); //创建新客人, 在创建中输出信息

					break;

				case 3: //客人离线
					//welive.flashTitle = 0; //声音等
					gid = data.g; type = 4; d = '此客人已离线!';
					offline.push(gid); //添加到离线客人数组中

					g_online.find('#g' + gid).addClass("offline");
					g_online.find('#g' + gid).children('.tip').addClass('tipOffline');
					g_online.find('#g' + gid).children('.closeGuest').addClass('show-close');
					break;

				case 11: //转接客人返回信息
					if(data.i == 1){ //转接成功
						gid = data.g; type = 3; d = '此客人转接成功.';
						offline.push(gid); //添加到离线客人数组中

						g_online.find('#g' + gid).addClass("offline");
						g_online.find('#g' + gid).children('.tip').addClass('tipOffline');
						$('.alert').addClass('alert-danger').html('已转接').show().delay(3000).fadeOut();
						g_online.find('#g' + gid).children('.closeGuest').addClass('show-close');
					}else{
						gid = data.g; type = 4; d = '此客人转接失败!';
					}

					break;

				case 13: //客人请求回拨电话
					gid = data.g;
					welive.flashTitle = 1; //声音等
					type = 2; //客人发来的
					d = '<div class="spec_info">' + data.i + '</div>';
					break;
			}
			break;

		case 7: //客人发送图片
			switch(data.a){

				case 2: //客人上传图片通知客服后返回的信息--表示上传成功

					gid = data.g;
					type = 2; //客人发来的
					welive.flashTitle = 1; //声音

					if(data.w <1) data.w = 1;
					var img_h = parseInt(data.h * 200 / data.w); //CSS样式中已确定宽度为200

					d = '<img src="' + SYSDIR + 'upload/img/' + data.i + '" class="receive_img" style="height:' + img_h + 'px;" onclick="show_big_img(this, ' + data.w + ', ' + data.h + ');">';

					break;

				case 4: //客服上传图片给客人成功后返回
					//gid = data.g;

					//因为可能同时有多个上传, 此处无法处理上传进度条等信息

					break;

			}

			break;
	}
	welive_output(d, gid, type); //输出

}

//显示大图片
function show_big_img(me, width, height){
	if(width/height >= 1200/700){
		var d_w = width;
		if(d_w > 1200) d_w = 1200;
		if(width < 1) width = 1;
		var d_h = height * d_w / width;
	}else{
		var d_h = height;
		if(d_h > 700) d_h = 700;
		if(height < 1) height = 1;
		var d_w = width * d_h / height;
	}

	easyDialog.open({
		container:{
			header: "图片",
			content: '<img src="' + me.src + '" style="width: ' + d_w + 'px;height: ' + d_h + 'px;" onclick="easyDialog.close();return false;">',
		},
		width: d_w + 20,
		height: d_h + 20,
		//lock: true, //禁用ESC键关闭, 因为ESC已经用于关闭客人小窗口
	});
}

//创建客人: 当客服刷新页面或断线重连时old = 1; 新客人进入时old指对话历史记录; au=上传授权authupload
function guest_create(gid, n, lang, old, au, mb, lastip, ipzone){
	if(!gid) return;
	welive.flashTitle = 1;  // flashtitle  // 网页标题
	var d, i = $.inArray(gid, offline);  // 查找客人是否在离线数组中 
    
	if(i > -1){ //表示客人重新上线
		d = '此客人重新上线了!';
		offline.splice(i, 1); //将其从离线数组中删除
		g_online.find('#g' + gid).removeClass("offline");  // 左边客人数组
		g_online.find('#g' + gid).children('.tip').removeClass('tipOffline');
		g_online.find('#g' + gid).children('.closeGuest').removeClass('show-close');
	
		//更新客人上传权限图片状态
		if(au && auth_upload){
			g_win.find(".t_authupload").hide();
			g_win.find(".t_offupload").show();
		}else if(auth_upload){
			g_win.find(".t_offupload").hide();
			g_win.find(".t_authupload").show();
		}

	}else if($.inArray(gid, guest) < 0){ //新客人
		guest.push(gid);

		//if(n == '') n = ((lang == 1)? '访客 '  : 'Guest ') + gid;
		if(n == '') n = ((lang == 1)? ipzone  : 'Guest ') + "(" + lastip + ")";
		  //   添加到左边的客人数组中的最前面----------------添加到左边客人
		g_online.prepend('<div id="g' + gid + '" onclick="openWin(' + gid + ');" class="g" title="' + n + '"><span class="tip"></span><i>' + n + '</i><b>0</b>' + ((mb == 1)? '<u></u>'  : '') + '<span class="closeGuest" title="关闭" onclick="closeWin(' + gid + ');">X</span>'+'</div>');

		var recs = '';
		if(old === 1){ //指客服重新上线
			d =  admin.fullname + ' 重新上线的通知已发送此客人!';
		}else{
			d = '客人上线, 问候语已发送!';
			if(old){ //如果有对话记录先输出 

				$.each(old, function(i, rec){

					//上传图片的记录
					if(rec.ft == 1){
						var img_arr = rec.m.split("|");
						var img_w = parseInt(img_arr[1]);
						var img_h = parseInt(img_arr[2]);

						if(img_w < 1) img_w = 1;
						var img_h_new = parseInt(img_h * 200 / img_w); //CSS样式中已确定宽度为200

						var rec_i = '<img src="' + SYSDIR + 'upload/img/' +img_arr[0] + '" class="receive_img" style="height:' + img_h_new + 'px;" onclick="show_big_img(this, ' + img_w + ', ' + img_h + ');">';
					}else{
						var rec_i = format_output(rec.m);
					}

					var position = "l"; //默认为客人的记录
					if(rec.t == 1)	position = "r"; //客服的记录

					//recs += '<div class="msg ' + position + '"><!--<b></b>--><div class="b"><div class="i">' + rec_i + '</div></div><p>' + rec.d + '</p></div>';
					recs += '<div class="msg ' + position + '"><div class="b"><div style="color:#6f6f6f;">' + rec.d + '</div><div class="i">' + rec_i + '</div></div></div>';

				});
			}
			if(recs != '') recs += '<div class="msg s"><div class="b"><div class="ico"></div><div class="i">... 以上为最近对话记录.</div></div></div>';;
		}
		createWin(gid, n, recs, lang, au); //创建窗口

	}else{ //客人重复连接
		welive.flashTitle = 0;
		return;
	}
	guest_output(d, gid, 3); //给客人小窗口输出信息
}

//客人增加信息数
function guest_update(gid){
	var o = g_online.find("#g" + gid + ">b");
	var x = parseInt(o.html());
	x = x + 1;
	o.html(x).show();
}

//删除客人并清除信息
function guest_delete(gid){
	$('#wicket'+gid).remove(); //清除聊天窗口
	var removeGid = g_online.find('#g' + gid);
	removeGid.slideUp(300, function () {//更新列表
        removeGid.remove();
    })
	guest.splice($.inArray(gid, guest), 1); //删除数组中的元素	
	offline.splice($.inArray(gid, offline), 1); //删除离线数组中的元素
}

//客人窗口输出信息
function guest_output(d, gid, type){
	if(!d || !gid || !type) return; //没有信息及类型返回
	TitleSound(1);
	var o = $("#win" + gid).find(".g_history");
	
	if(type != 1) o.find(".overview>div.updating").remove(); //删除输入状态
	switch(type){
		case 1: //客服
			d = '<div class="msg r"><div class="b"><div style="color:#6f6f6f;">' + getLocalTime() + '</div><div class="i">' + format_output(d) + '</div></div></div>';
			break;
		case 2: //客人
			d = '<div class="msg l"><div class="b"><div style="color:#6f6f6f;">' + getLocalTime() + '</div><div class="i">' + format_output(d) + '</div></div></div>';
			break;
		case 3: //正常提示
			d = '<div class="msg s"><div class="b"><div class="ico"></div><div class="i">' + d + '</div></div></div>';
			break;
		case 4: //错误提示
			d = '<div class="msg e"><div class="b"><div class="ico"></div><div class="i">' + d + '</div></div></div>';
			break;
		case 5: //客服
			d = '<div class="msg r"><div class="b"><div style="color:#6f6f6f;">' + getLocalTime() + '</div><div class="i">' + d + '</div></div></div>';
			break;
	}
	$("#wicket"+ gid).children().append(d);
	if(!CurrentId || !welive.where){
		openWin(gid); //如果没有打开小窗口, 或者welive位置在群聊区, 自动弹出
	}else{
		if(CurrentId != gid) guest_update(gid); //其它情况, 增加未读消息数量
	}
	scrollbar(CurrentId);// 更新当前窗口id滚动条
}

//客服交流输出信息
function welive_output(d, gid, type){
	if(gid){
		guest_output(d, gid, type);
	}else{
		if(d === false) return; //没有信息返回
		TitleSound();
		s_history.append(d);
		s_hwrap.welivebar_update('bottom'); //滚动到底部
	}
}

//客服连接验证
function welive_verify(){
	welive.status = 1;
	welive_send('x=2&a=8&s=' + admin.sid + '&ag=' + admin.agent + '&id=' + admin.id + '&fr=0');

	//将挂起及解除挂起按钮恢复
	$(".set_serving").hide();
	$(".set_busy").show();
}

//连接断开时执行
function welive_close(){
	if(welive.status){ //之前已连接时
		//尝试重新打开socket服务, 因为如果只有一个客服存在并断线，socket进程会终止
		if(welive.autolink) $.ajax({url: './index.php?c=opensocket&a=ajax', dataType: 'json', async: true, cache: false});

		s_online.html(""); //清空客服在线列表
		s_admins.html(0); //将客服在线人数清0
	}

	welive.status = 0;
	clearInterval(ttt_1); //连接断开后停止发送心跳数据

	$("#websocket1212").remove(); //如果不清除, IE下有冲突

	//允许重新连接
	if(welive.autolink){
		welive_output('<div class="i"><b></b>连接失败, 3秒后自动重试 ...</div>');
		welive.ttt = setTimeout(welive_link, 3000);
	}
}

//群发送信息
function welive_send(d){
	s_send.addClass('loading2');
	if(welive.status) {
		welive.ws.send(d);
		s_msg.val('');
	}else if(welive.autolink){
		welive_output('<div class=i><b></b>服务器连接中, 请等待 ...</div>');
	}
	s_msg.focus();
	s_send.removeClass('loading2');
}

//给客人发信息
function guest_send(){  
	if(guest==null || (guest!=null && guest.length==0)) {
		$('.alert').addClass('alert-warning').html('未有访客无法开启对话').show().delay(3000).fadeOut();
	};
	// if(!CurrentId || $.inArray(CurrentId, offline) > -1) return; //客人离线无法发送
	var o = $('#wicket' + CurrentId).parent().parent().find('.g_msg'); 
	var msg = $.trim(o.val());  // 去除两端空白字符 发送消息
	if(welive.status && msg){ 
		msg = msg.replace(/&/g, "||4||"); //将&转换成特殊标记, 否则与传送的其它数据冲突
		welive.ws.send('x=5&g=' + CurrentId + '&i=' + msg);
		o.val('');
	}
	o.select();
}

//上传图片监听
function sendImgFileListener(obj, fun) {
	$(obj).change(function() {
		try {
			var file = this.files[0];
			var reader = new FileReader();

			reader.onload = function(e) {

				var img = new Image();
				img.src = reader.result;

				img.onload = function() {
					var w = img.width,
						h = img.height;

					if(file.type == 'image/gif'){

						var base64 = e.target.result; //可保持gif动画效果

					}else{

						var canvas = document.createElement('canvas');

						var ctx = canvas.getContext('2d');
						$(canvas).attr({width: w,	height: h});
						ctx.drawImage(img, 0, 0, w, h);

						var base64 = canvas.toDataURL(file.type, 0.6); //canvas对JPG图片有压缩效果, gif, png
					}

					var result = {
						url: window.URL.createObjectURL(file),
						w: w,
						h: h,
						size: file.size,
						type: file.type,
						base64: base64.substr(base64.indexOf(',') + 1),
					};

					fun(result);
				};

				img.onerror = function() {
					showInfo("图片文件格式无效", "发送图片", "", 3, 0);
				};

			};

			reader.readAsDataURL(file);

		} catch(e) {
			//
		}
	});
}

//传送图片, data对象属性: size文件大小(字节), type(文件类型), base64(图片纯净的base64代码)
function welive_send_img(data){
	$("#upload_img").val("");

	//限定文件大小及类型
	if(data.size > 1024 * 2000){
		showInfo("图片文件不能超过2M", "发送图片", "", 3, 0);
		return;
	}

	if($.inArray(data.type, ["image/jpg","image/jpeg","image/png","image/gif"]) < 0) {
		showInfo("图片文件格式无效!", "发送图片", "", 3, 0);
		return;
	}

	if(!welive.status || !CurrentId || $.inArray(CurrentId, offline) > -1) return;  //客人不在线或连接失败时返回


	var gid = CurrentId;
	var sending_img_w = data.w; //图片的宽度
	var sending_img_h = data.h; //图片的宽度

	if(sending_img_w < 1) sending_img_w = 1;
	var sending_mask_h = parseInt(sending_img_h * 200 / sending_img_w); //CSS样式中已确定宽度为200
	
	var img_str = '<div class="sending_div" style="height:' + sending_mask_h + 'px;"><img src="' + data.url + '" class="sending_img" onclick="show_big_img(this, ' + data.w + ', ' + data.h + ');"><div class="sending_mask" style="line-height:' + sending_mask_h + 'px;height:' + sending_mask_h + 'px;">上传中 ...</div></div>';
	
	guest_output(img_str, gid, 5);

	//显示上传进度
	var sending_mask = $("#win" + gid).find(".g_history").find(".sending_mask").last(); //仅处理最后一次上传的图片
	var ttt_2 = setInterval(function(){
		if(sending_mask_h < 40){
			clearInterval(ttt_2);
			return;
		}else{
			sending_mask_h -= 20;
			sending_mask.css({"height":sending_mask_h + "px", "line-height":sending_mask_h + "px"});
		}
	}, 100);


	//ajax上传文件
	$.ajax({
		url: './index.php?c=upload_img&a=ajax&action=upload',
		data: {img_type: data.type, img_base64: data.base64},
		type: "post",
		cache: false,
		async: true,
		dataType: "json",
		success: function(data){
			if(data.s == 1 && welive.status){ //上传成功后
				
				//返回的文件名发送给客人
				welive.ws.send("x=7&a=4&g=" + gid + "&i=" + data.i + "&w=" + sending_img_w + "&h=" + sending_img_h); //w,h指图片的宽,高(像素)
				clearInterval(ttt_2);
				sending_mask.remove();

			}else{
				clearInterval(ttt_2); //上传进度停止
				guest_output(data.i, gid, 4); //输出服务器返回的错误信息
				sending_mask.html("上传失败").css({"color":"red", "font-weight":"bold"});

				$('#win' + gid).find('.g_msg').select();
			}
		},
		error: function(XHR, Status, Error) {
			showInfo("上传图片时发生ajax错误!", "发送图片", "", 8, 0);
			//alert("Data: " + XHR.responseText + "\r\nStatus: " + Status + "\r\nError: " + Error);
		}
	});

}

//粘贴图片文件给客人
function pasteImg(e){
	//if($.inArray(CurrentId, offline) > -1) return;  //客人不在线时
	var isChrome = false;
	if(e.clipboardData || e.originalEvent){
		var clipboardData = (e.clipboardData || e.originalEvent.clipboardData);
		if(clipboardData.items){
			var items = clipboardData.items,len = items.length,blob = null;
			isChrome = true;
			//e.preventDefault();
			var file;
			for(var i=0;i<len;i++){
				if(items[i].type.indexOf("image") !== -1){
					blob = items[i].getAsFile();
				}
			}
			if(blob !== null){
				var reader = new FileReader();
				reader.onload=function(event){
					var img = new Image();
					img.src = reader.result;
					img.onload = function() {
						var w = img.width,h = img.height;
						if(blob.type == 'image/gif'){
							var base64 = e.target.result; //可保持gif动画效果
						}else{
							var canvas = document.createElement('canvas');
							var ctx = canvas.getContext('2d');
							$(canvas).attr({width: w,	height: h});
							ctx.drawImage(img, 0, 0, w, h);
							var base64 = canvas.toDataURL(blob.type, 0.6); //canvas对JPG图片有压缩效果, gif, png
						}
						var result = {
							url: window.URL.createObjectURL(blob),
							w: w,
							h: h,
							size: blob.size,
							type: blob.type,
							base64: base64.substr(base64.indexOf(',') + 1),
						};
						welive_send_img(result);
					}
				}
				reader.readAsDataURL(blob);
			}
		}
	}
}

//上传图片文件给客人
function upload_photo(){
	if($.inArray(CurrentId, offline) > -1) return;  //客人不在线时
	if(welive.status){
		document.getElementById("upload_img").click(); //兼容IE
	}
	return false;
}

//踢出客人
function guest_kickout(){
	if($.inArray(CurrentId, offline) < 0) welive.ws.send('x=6&a=6&g=' + CurrentId); //客人在线, 发送踢出请求
	guest_delete(CurrentId);
	CurrentId = 0;
	$("#tiptip_holder").hide();
	// showNext();
}

//禁止发言
function guest_banned(){
	if($.inArray(CurrentId, offline) > -1) return;
	welive.ws.send('x=6&a=7&g=' + CurrentId); //发送请求
	guest_output('此客人已被禁言, 但你仍然可以对其发言!', CurrentId, 4);
	$("#tiptip_holder").hide();
	var curr_win = $("#win" + CurrentId);
	curr_win.find(".t_banned").hide();
	curr_win.find(".t_unban").show();
	curr_win.find('.g_msg').select();
}

//解除禁言
function guest_unban(me){
	if($.inArray(CurrentId, offline) > -1) return;
	welive.ws.send('x=6&a=10&g=' + CurrentId); //发送请求
	guest_output('此客人禁言状态已解除.', CurrentId, 3);
	$(me).parent().children(".t_banned").show();
	$(me).hide();

	$('#win' + CurrentId).find('.g_msg').select();
}

//授权上传图片文件
function guest_authupload(){
	if($.inArray(CurrentId, offline) > -1) return;
	welive.ws.send('x=7&a=1&g=' + CurrentId); //授权请求
	guest_output('已授权此客人上传图片文件权限!', CurrentId, 3);
	$("#tiptip_holder").hide();
	var curr_win = $("#win" + CurrentId);
	curr_win.find(".t_authupload").hide();
	curr_win.find(".t_offupload").show();
	curr_win.find('.g_msg').select();
}

//解除上传授权
function guest_offupload(){
	//客人不在线仍然可以解除授权
	//if($.inArray(CurrentId, offline) > -1) return;
	welive.ws.send('x=7&a=3&g=' + CurrentId); //解除请求
	guest_output('此客人的上传权限已被解除!', CurrentId, 4);
	$("#tiptip_holder").hide();
	var curr_win = $("#win" + CurrentId);
	curr_win.find(".t_offupload").hide();
	curr_win.find(".t_authupload").show();
	curr_win.find('.g_msg').select();
}

//获得在线客服列表, tipTip中使用
function get_supporters(){
	if($.inArray(CurrentId, offline) > -1){
		$('.s_transfer').html('此客人已离线, 无法转接!');
		return;
	}
	var num = 0;
	$('.s_transfer').html(s_online.html()).children('li').each(function() {
		var aix = $(this).attr('id').replace('index_', '');
		if(admin.aix == aix || aix == "robot"){
			$(this).remove(); //去掉自己或机器人
			return;
		}
		num += 1;
		$(this).click(function(){
			if($.inArray(CurrentId, offline) > -1){
				guest_output('此客人已离线, 无法转接!', CurrentId, 4);
				return;
			}
			welive.ws.send('x=6&a=11&g=' + CurrentId + '&aix=' + aix); //发送转接请求
			$("#tiptip_holder").hide();
		});
	});
	if(!num) $('.s_transfer').html('暂无其它客服可转接!');
}

//页面加载获取客人数据
function get_guestInfo(id){
	if(id == 0) return;
	welive.ws.send('x=2&a=5&g=' + id);
}

//跳转到客人聊天记录
function get_records(record){
	var gid = $(record).parent().parent().parent().parent().parent().attr("id");
	if(gid == 'myRecord0' || !gid) return; // 没选中客人不能访问聊天记录
	var guestId = gid.substr(8,gid.length-1);
	var gId = Number(guestId);
	$("#my_history").show();
	$("#my_historys").attr('src', 'index.php?c=messages&s=' + gId);
	// 历史记录窗口拖拽
	$('#my_history').mousedown(function(e) {
		var positionDiv = $(this).offset();
		var distenceX = e.pageX - positionDiv.left;
		var distenceY = e.pageY - positionDiv.top;
		$(document).mousemove(function(e) {
			var x = e.pageX - distenceX;
			var y = e.pageY - distenceY;
			if (x < 0) {
				x = 0;
			} else if (x > $(document).width() - $('#my_history').outerWidth(true)) {
				x = $(document).width() - $('#my_history').outerWidth(true);
			}
			if (y < 0) {
				y = 0;
			} else if (y > $(document).height() - $('#my_history').outerHeight(true)) {
				y = $(document).height() - $('#my_history').outerHeight(true);
			}
			$('#my_history').css({
				'left': x + 'px',
				'top': y + 'px'
			});
		});
		$(document).mouseup(function() {
			$(document).off('mousemove');
		});
	});
	var my_history_i = $('#my_history').find('i');
	$(my_history_i).click(function(){
		$("#my_history").hide();
	});
} 

//保存当前客人数据
function guest_save(me){
	//serialize会将空格变成+号, 恢复之;  &会变成%26, 转换成||4||
	var data = $(me).closest("form").serialize().replace(/\+/g, " ").replace(/\%26/g, "||4||");
	welive.ws.send('x=2&a=6&g=' + CurrentId + '&' + data); //发送保存数据请求
}

//客人输入状态更新
function welive_runtime(gid, msg){
	if(!gid || !msg) return;
	msg = format_output(msg) + ' <img src="' + SYSDIR + 'public/img/writting.gif">';
   	var o = $("#win" + gid).find(".g_history");
	var updating = o.find(".overview>div.updating");
	if(updating.length){
		updating.find(".i").html(msg); //之前存在

	}else{
		msg = '<div class="msg updating"><!--<b></b>--><div class="b"><div class="i">' + msg + '</div></div></div>';
		$("#wicket"+ gid).children().append(msg);
	}
	scrollbar(gid) //更新滚动条
}


//welive初始化
function welive_init(){
	guest = new Array();  // 客人
	offline = new Array(); //已离线的客人数组
	g_online = $("#g");  // 获取左边客户数组

	s_chat = $("#s_chat");
	s_msg = s_chat.find(".s_msg");
	s_send = s_chat.find(".s_send");
	s_admins = s_chat.find(".s_admins");
	s_title = s_chat.find(".s_title").children(".l");
	s_hwrap = s_chat.find("#s_hwrap");
	s_owrap = s_chat.find("#s_owrap");
	s_history = s_hwrap.find(".overview");
	s_online = s_owrap.find(".overview");
	sounder = $("#wl_sounder");
	v_height = $("#x-bodymid");
	record_height = $(".fast-reply");
	l_note_height = $(".l_note");

	welive_link(); //socket连接
	var s_historyViewport = s_hwrap.find(".viewport"),
	s_onlineViewport = s_owrap.find(".viewport"),
	xHeight = $(window).height() - 88;

	g_online.height(xHeight);
	s_chat.height(xHeight);
	s_historyViewport.height(xHeight - 78);
	s_onlineViewport.height(xHeight - 74);
	v_height.height(xHeight - 31);
	record_height.height(xHeight -67);
	l_note_height.height(xHeight -71);

	s_hwrap.welivebar();
	s_owrap.welivebar();
	$(window).resize(function(){//窗口大小改变时
		var wh= $(window).height()-88;
		g_online.height(wh);
		s_chat.height(wh);
		s_historyViewport.height(wh - 78);
		s_onlineViewport.height(wh - 74);
		v_height.height(wh - 31);
		record_height.height(wh - 67);
		l_note_height.height(wh - 71);
		s_hwrap.welivebar_update('bottom');
		s_owrap.welivebar_update();
	});

	s_msg.keydown(function(e){
		if(e.keyCode ==13) s_send.trigger("click");
	}).focus(function(){
		s_title.removeClass("off"); //群聊区选中
		welive.where = 0;
	});
     // 客人区Enter发送
	$('.mywin').find(".g_msg").keydown(function(e){
		var e=window.event||e; 
        var code = e.keyCode || e.which || e.charCode;
		var shift = e.shiftKey;
		var code = e.keyCode;
		if(shift && code === 13) {
			return;
		}else if(code === 13) {
			guest_send();
			return false;
		}	
	});
	 //输入框粘贴图片
	$('.mywin').find(".g_msg").bind({paste:pasteImg});
      // 切换快捷回复和客人信息
	$(".apply-record .tab .tab-item").click(function() {
		$(this).addClass("active").siblings().removeClass("active");
		$(".recordContent .mainCont").eq($(this).index()).show().siblings().hide();
	})
	 // 群聊区--发送信息
	s_send.click(function(e) {
		var msg = $.trim(s_msg.val());
		if(msg){
			msg = msg.replace(/&/g, "||4||"); //将&转换成特殊标记, 否则与传送的其它数据冲突
			welive_send('x=1&i=' + msg);
		}else{
			s_msg.focus();
		}
		e.preventDefault();
	});

	//声音
	$("#wl_ring").click(function(e) {
		if(welive.sound){
			welive.sound = 0;
			$(this).addClass("s_ringoff").removeClass("s_ring");
		}else{
			welive.sound = 1;
			$(this).addClass("s_ring").removeClass("s_ringoff");
		}
		s_msg.focus();
		e.preventDefault();
	});

	//右边管理员 区表情符号
	s_chat.find(".s_face").tipTip({content: $(".smilies_div").html(), keepAlive:true, maxWidth:"242px", defaultPosition:"top", edgeOffset:-31, delay:300});

	//无人值守, 挂起, 解除挂起, 重启服务等提示
	$(".set_busy").tipTip({content: '1. 挂起后, 将不再接受新客人加入, 但其他客服转接过来的客人仍会进入.<br><br>2. 一般地, 当自己特别忙时可使用挂起功能, 如果离开座席较长时间, 建议退出客服.<br><br>3. 如果所有在线的客服都挂起了, 挂起功能将失效.'});

	$(".sysinfo>a").tipTip({content: '1. 按 Ctrl + Alt: 在客服交流区与当前客人小窗口间切换.<br>2. 按 Ctrl + 下箭头 或 Esc键: 关闭当前客人小窗口.<br>3. 按 Ctrl + 上箭头: 展开关闭的客人小窗口.<br>4. 按 Ctrl + 左或右箭头: 在已展开的客人小窗口间切换.<br>5. 客人被踢出或禁言后, 其刷新页面仍可重新进入客服.<br>6. 客人获取上传授权后将会一直保留, 直至被客服解除.', maxWidth:"320px", defaultPosition:"top", delay:300});

	$(".set_serving").tipTip();


	//管理员专有
	if(admin.type == 1){
		$(".set_robot_on").tipTip({content: '1. 开启无人值守后, 新上线的访客将由机器人自动回复, 已在线的访客仍由原客服提供服务.<br><br>2. 开启无人值守后, 所有客服均不再接受新客人, 且可离线.<br><br>3. 开启无人值守后, 访客留言功能将失效.<br><br>4. 只有关闭无人值守, 新登录的客服才能提供在线服务.<br><br>5. Socket服务重启后, 无人值守状态需要重新设置.'});

		$(".set_robot_off, .reset_socket").tipTip();
	}

	//监听上传图片控件
	sendImgFileListener("#upload_img", function(data) {
		welive_send_img(data);
	});

	//关闭声音, 停止闪烁, 快捷键等
	pagetitle = document.title;
	$(document).mousedown(stopFlashTitle).keydown(function(e){
		stopFlashTitle();
		if(e.which == 27 || (e.ctrlKey && e.which == 40)) { //Esc 或 ctrl + 下箭头
			closeWin(CurrentId);
		}else	if(e.ctrlKey && (e.which == 37 || e.which == 39)) { //ctrl + 左右箭头
			// showNext();
		}else	if(e.ctrlKey && e.which == 38) { //ctrl + 上箭头
			// showNext(1);
		}else	if(e.ctrlKey && e.which == 18) { //ctrl + alt
			if(CurrentId){
				if(welive.where == 1){
					s_msg.focus();
				}else{
					openWin(CurrentId);
				}
			
			}else{
				s_msg.focus(); //如果没有打开小窗口, 群聊输入框直接获得焦点
			}
		}
	});

	//两种
	welive.sound1 = '<audio src="' + SYSDIR + 'public/sound1.mp3" autoplay="autoplay"></audio>';
	welive.sound2 = '<audio src="' + SYSDIR + 'public/sound2.mp3" autoplay="autoplay"></audio>';

	window.onbeforeunload=function(event){return " ";}; //离开当前页面时提示和选择
	$(window).unload(function(){welive.status=0;clearTimeout(welive.ttt);clearInterval(ttt_1);}); //关闭自动重连

	//每20分钟清除过长的客服间对话记录
	setInterval(welive_clear, 1000*1200);
}

//定义全局变量
var tttt = 0, ttt_1 = 0, pagetitle, flashtitle_step = 0, sounder, towhere = 0, flashtitle_step_a = 0;
var guest, offline, g_online, s_chat, s_msg, s_history, s_online, s_send, s_hwrap, s_owrap, s_admins, s_title;
var welive = {ws:{}, index: 0, status: 0, autolink: 1, ttt: 0, flashTitle: 0, sound: 1, sound1: '', sound2: '', where: 0, is_robot: 0};
var CurrentId = 0;
var x_win_content = '<div class="viewport"><div class="overview">888888</div></div>';

$(function(){
	$.ajax({url: './index.php?c=opensocket&a=ajax', dataType: 'json', async: true, cache: false}); //如果未开启, 打开socket服务
	welive_init(); //welive初始化
	$("#topbar dl").Jdropdown({delay: 50}, function(a){});
	//退出登录
	$(".logout").click(function(e) {
		showDialog('确定退出 BayLive 在线客服系统吗?', '', function(){
			document.location = 'index.php?a=logout';
		});

		e.preventDefault();
	});
});
