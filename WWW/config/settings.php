<?php if(!defined('ROOT')) die('Access denied.');

$_CFG = array();
$_CFG['BaseUrl'] = "http://localhost/";
$_CFG['Actived'] = "1";
$_CFG['ColorStyle'] = "2";
$_CFG['History'] = "1";
$_CFG['Record'] = "10";
$_CFG['Lang'] = "Chinese";
$_CFG['AuthUpload'] = "0";
$_CFG['SocketPort'] = "8432";
$_CFG['AutoOpen'] = "0";
$_CFG['Update'] = "5";
$_CFG['DeleteHistory'] = "0";
$_CFG['AutoOffline'] = "6";
$_CFG['KillRobotCode'] = "2T161F68";
$_CFG['Timezone'] = "+8";
$_CFG['DateFormat'] = "Y-m-d";
$_CFG['Title'] = "开元在线客服";
$_CFG['Welcome'] = "[:8:]很高兴为您服务，有什么可以协助您的呢？";
$_CFG['Welcome_en'] = "[:8:]很高兴为您服务，有什么可以协助您的呢？";
$_CFG['Comment_note'] = "提供您的会员账号，能让我们快速为您查看解决问题哦~<br/>[:8:]很高兴为您服务，有什么可以协助您的呢？";
$_CFG['Comment_note_en'] = "提供您的会员账号，能让我们快速为您查看解决问题哦~<br/>[:8:]很高兴为您服务，有什么可以协助您的呢？";
$_CFG['Prevent'] = 838273234;
$_CFG['AppName'] = "6LSd6IGK";
$_CFG['CopyrightUrl'] = "aHR0cDovL3d3dy5iYXktbGl2ZS5jb20v";
$_CFG['Email'] = "xxxx@xxxxxx.com";
$_CFG['UseSmtp'] = "1";
$_CFG['SmtpHost'] = "smtp.163.com";
$_CFG['SmtpEmail'] = "xxxxxx@163.com";
$_CFG['SmtpPort'] = "25";
$_CFG['SmtpUser'] = "xxxxxxxx";
$_CFG['SmtpPassword'] = "xxxxxxxxx";
$_CFG['RobotName'] = "小甜";
$_CFG['RobotName_en'] = "Lai Lai";
$_CFG['RobotPost'] = "开元";
$_CFG['RobotPost_en'] = "Smart Robot Support";

?>