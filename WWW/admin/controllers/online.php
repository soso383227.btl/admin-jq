<?php if(!defined('ROOT')) die('Access denied.');

class c_online extends Admin{

	public function __construct($path){
		parent::__construct($path);

	}


	public function index(){
		$smilies = ''; //表情图标
		for($i = 0; $i < 24; $i++){
			$smilies .= '<img src="' . SYSDIR . 'public/smilies/' . $i . '.png" onclick="insertSmilie(' . $i . ', towhere);">';
		}

		$myid = $this->admin['aid'];
		$phrases = ''; //中文常用短语
		$phrases_en = ''; //英文常用短语
		$getphrases = APP::$DB->getAll("SELECT msg, msg_en FROM " . TABLE_PREFIX . "phrase WHERE aid = '$myid' AND activated =1 ORDER BY sort LIMIT 10");

		$getphrases = array_reverse($getphrases, true); //数组反转一下
		foreach($getphrases AS $k => $phrase){
			$phrases .= '<li onclick="insertPhrase(this, towhere);"><b>' . $phrase['msg'] . '</b></li>';//<i>' . ($k+1) . ')</i>
			$phrases_en .= '<li onclick="insertPhrase(this, towhere);"><b>' . $phrase['msg_en'] . '</b></li>';//<i>' . ($k+1) . ')</i>
		}

		if(!$phrases) {
			$phrases = '<li>暂未添加常用短语!</li>';
			$phrases_en = '<li>暂未添加常用短语!</li>';
		}

		echo    
		    '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="220px"><div id="g"></div></td>
					<td width="53%;">
					    <div class="mywin">
							<div class="x-title">
								<div class="x-titlemid"></div> 
							</div> 
							<div class="x-body">
								<div class="x-bodymid" id="x-bodymid">
									<div class="g_history">
                                      
									</div>
									<div class="g_tools">
										<a class="t_smilies" title="表情符号"></a>
										<a class="t_photo" title="发送图片" onclick="upload_photo();return false;"></a>
										<a class="t_kickout" title="踢出客人" style="display:none;"></a><a class="t_authupload" title="授权上传图片文件"></a>
										<a class="t_banned" title="禁止发言" style="display:none;"></a>
										<a class="t_unban" title="解除禁言" onclick="guest_unban(this);return false;" style="display:none;"></a>
										<a class="t_transfer" title="转接客人"></a>
										<a class="t_offupload" title="解除上传授权"></a>
									</div>
									<div class="g_bott" style="height:20%; text-align:center;">
										<textarea name="g_msg" id="text_in" class="g_msg" 
											style="font-weight: normal; font-size: 16px; 
											overflow: hidden; word-break: break-all; 
											font-style: normal;outline: none; border:none;
											contenteditable="true"; tabindex="0";></textarea>   
									</div>
									<div class="guest-send">
										<a class="g_send" title="Enter发送/Enter+Shift换行" onclick="guest_send();return false;">发送</a>
							        </div>
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="apply-record">
							<ul class="tab">
								<li class="tab-item active">客人信息</li>
								<li class="tab-item">快捷回复</li>
							</ul>
							<div class="recordContent">
								<div class="mainCont selected">
									<div class="l_note" loaded="0">
										<form onsubmit="return false;">
											<li class="f">
												<b>来源:</b>
												<u><a href="" title="" target="_blank" class="fromurl" style="display: block;"></a></u>
											</li>
											<li class="f"><b>地区:</b><u class="ipzone" title=""></u>
											</li>
											<li class="f">
												<b>意向:</b>
												<label><input type="radio" value="1" name="grade"><i>1分</i></label>
												<label><input type="radio" value="2" name="grade"><i>2分</i></label>
												<label><input type="radio" value="3" name="grade"><i>3分</i></label>
												<label><input type="radio" value="4" name="grade"><i>4分</i></label>
												<label><input type="radio" value="5" name="grade"><i>5分</i></label>
											</li>
											<li class="chatting-records"><b>记录:</b><a title="查看聊天记录" onclick="get_records(this);return false;">查看聊天记录</a></li>
											<li><b>姓名:</b><input name="fullname" type="text" class="s"></li>
											<li><b>电话:</b><input name="phone" type="text" class="l"></li>
											<li><b>Email:</b><input name="email" type="text" class="l"></li>
											<li><b>地址:</b><input name="address" type="text" class="l"></li>
											<li><b>备注:</b><textarea name="remark"></textarea></li>
											<li class="bt"><input class="cancel" type="submit" value="保存更新" onclick="guest_save(this);return false;"></li>
										</form>
									</div>
								</div>
								<div class="mainCont">
									<div class="fast-reply">'. $phrases .'</div>
								</div>
							</div>
						</div>
					</td>
					<td style="width: 20%;">
						<div id="s_chat">
							<div class="s_title"><div class="l">内部交流</div><div class="r"><b class="s_admins">0</b> 位在线</div></div>
							<div class="s_mid">
								<div id="s_hwrap" class="loading3">
									<div class="scb_scrollbar scb_radius">
										<div class="scb_tracker">
											<div class="scb_mover scb_radius"></div>
										</div>
									</div>
									<div class="viewport">
										<div class="overview">
											<div class="i" style = "color:#FF0000;width:228px;"><b></b>1.所有电脑都不允许接收任何文件。<br/>
						2.无论谁发QQ/客服说是老板或其它方式要钱都不允许转钱，任何金额没有熊猫或者老鹰电话确认的都不能转。没有确认转出的需自行承担损失！<br/>
						3.对外不能泄露公司网址及其它信息<br/>
						4.出款人员出款操作不能使用名字搜索，必须从后台复制再粘贴。<br/>
						请大家互相转告督促 ...</div>
										</div>
									</div>
								</div>
								<div id="s_owrap">
									<div class="scb_scrollbar scb_radius"><div class="scb_tracker"><div class="scb_mover scb_radius"></div></div></div>
									<div class="viewport">
										<div class="overview"></div>
									</div>
								</div>
							</div>
							<div class="s_bott">
								<div class="s_face"></div>
								<input name="s_msg" placeholder="Enter to send" type="text" class="s_msg">
								<a class="s_send" title="发送"></a>
								<a id="wl_ring" class="s_ring" title="声音"></a>
							</div>
						</div>
					</td>
				</tr>
				</table>
				<input type="file" name="file" id="upload_img" accept="image/jpg,image/jpeg,image/gif,image/png" style="width:1px;height:1px;display:none;overflow:hidden;">
				<div id="wl_sounder" style="width:1px;height:1px;visibility:hidden;overflow:hidden;"></div>
				<div class="smilies_div" style="display:none"><div class="smilies_wrap">' . $smilies . '</div></div>
				<div id="my_history">
					<div>
						<i title="关闭">X</i>
					</div>
					<iframe name="my_historys" id="my_historys" src="" width="99.5%" height="95%" style="overflow:auto"></iframe>
				</div>
				<div class= "alert"></div>';
	}

} 

?>