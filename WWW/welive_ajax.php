<?php  

define('ROOT', dirname(__FILE__).'/');  //系统程序根路径, 必须定义, 用于防翻墙

require(ROOT . 'includes/core.guest.php');  //加载核心文件
require(ROOT . 'includes/functions.ajax.php');  //加载需要的函数

if(!$_CFG['Actived']) die("Access denied");

//ajax操作
$ajax =  intval($_GET['ajax']);

if(!$ajax) die("Access denied");


if($dbmysql == "mysqli"){
	$DB = new DBMysqli($dbusername, $dbpassword, $dbname,  $servername, false, false); //MSQLI, 不显示mysql查询错误
}else{
	$DB = new DBMysql($dbusername, $dbpassword, $dbname,  $servername, false, false);
}

$dbpassword = '';


$json = new JSON;
$ajax = array('s' => 0, 'i' => '');

$act = ForceStringFrom('act');

//访客上传图片
if($act == "uploadimg"){
	$img_path = ROOT . 'upload/img/'; //保存图片的目录
	$img_array = array("image/jpg" => '.jpg', "image/jpeg" => '.jpg', "image/png" => '.png', "image/gif" => '.gif');

	$key = ForceStringFrom('key');
	$code = ForceStringFrom('code');
	$decode = authcode($code, 'DECODE', $key);

	$gid = ForceIntFrom('gid');
	$img_type = strtolower(ForceStringFrom('img_type'));
	$img_base64 = $_POST['img_base64']; //图片文件编码内容不过虑

	//文件目录是否可写
	if(!is_writable($img_path)){
		$ajax['i'] = "目录不可写";
		die($json->encode($ajax));
	}

	//验证数据是否存在
	if(!$gid OR !$img_type OR !$img_base64){
		$ajax['i'] = $langs['badcookie'];
		die($json->encode($ajax));
	}

	//验证码是否过期
	if($decode != md5(WEBSITE_KEY . $_CFG['KillRobotCode'])){
		$ajax['i'] = $langs['badcookie'];
		die($json->encode($ajax));
	}

	//文件限制不能大于2M(前端js是按文件大小限制为1024 * 2000, 此处对应字节数约为1024 * 4000)
	if(strlen($img_base64) > 1024 * 10000){
		$ajax['i'] = $langs['img_limit'];
		die($json->encode($ajax));
	}

	//图片文件类型限制
	if(!array_key_exists($img_type, $img_array)) {
		$ajax['i'] = $langs['img_badtype'];
		die($json->encode($ajax));
	}

	//判断客人是否存在及是否有上传文件授权
	$session = ForceCookieFrom(COOKIE_USER . '_sess');
	if(!$session) $session = ForceStringFrom('sess'); //解决safari无法读取第三方cookie的问题(iframe)

	$guest = $DB->getOne("SELECT aid, upload, session FROM " . TABLE_PREFIX . "guest WHERE gid = '$gid'");
	if(!$guest OR !$guest['aid'] OR !$session OR $session != $guest['session']) {
		$ajax['i'] = $langs['no_upload_auth'];
		die($json->encode($ajax));
	}

	//上传图片需要客服授权
	if($_CFG['AuthUpload'] AND !$guest['upload']){
		$ajax['i'] = $langs['no_upload_auth'];
		die($json->encode($ajax));
	}

	//限制每小时最多仅可以上传20张图片
	$max_upload = 20;
	$sql_time = time() - 3600;
	$result = $DB->getOne("SELECT COUNT(mid) AS nums FROM " . TABLE_PREFIX . "msg WHERE type = 0 AND fromid = '{$gid}' AND time > '{$sql_time}'");
	if($result AND $result['nums'] >= $max_upload){
		$ajax['i'] = $langs['upload_alert'];
		die($json->encode($ajax));
	}


	//以当前时间的小时数为最后目录
	$current_dir = gmdate('Ym/d', time() + (3600 * ForceInt($_CFG['Timezone']))) . "/"; 
	if(!file_exists($img_path . $current_dir)){
		mkdir($img_path . $current_dir, 0777, true);
		@chmod($img_path . $current_dir, 0777);
	}

	//产生一个独特的文件名称, 包括小时目录
	$filename = $current_dir . md5(uniqid(COOKIE_KEY.microtime())) . $img_array[$img_type]; 

	if(file_put_contents($img_path . $filename, base64_decode($img_base64))){
		$ajax['s'] = 1;
		$ajax['i'] = $filename; //返回文件名, 包含时间日期目录
		die($json->encode($ajax));
	}

	$ajax['i'] = $langs['upload_failed'];
	die($json->encode($ajax));
}

//访客留言
if($act == "comment"){
	$key = ForceStringFrom('key');
	$code = ForceStringFrom('code');
	$decode = authcode($code, 'DECODE', $key);
	if($decode != md5(WEBSITE_KEY . $_CFG['KillRobotCode'])){
		die($json->encode($ajax)); //验证码过期
	}

	$fullname = ForceStringFrom('fullname');
	$email = ForceStringFrom('email');
	$phone = ForceStringFrom('phone');
	$content = ForceStringFrom('content');
	$vid = ForceIntFrom('vid');
	$vvc = ForceIntFrom('vvc');

	if(!$fullname OR strlen($fullname) > 90){
		$ajax['s'] = 2;
		die($json->encode($ajax));
	}elseif(!IsEmail($email)){
		$ajax['s'] = 3;
		die($json->encode($ajax));
	}elseif(!$content OR strlen($content) > 1800){
		$ajax['s'] = 4;
		die($json->encode($ajax));
	}elseif(!checkVVC($vid, $vvc)){
		$ajax['s'] = 5;
		die($json->encode($ajax));
	}

	$gid = ForceIntFrom('gid');
	$ip = GetIP();

	$DB->exe("INSERT INTO " . TABLE_PREFIX . "comment (gid, fullname, ip, phone, email, content, time) VALUES ('$gid', '$fullname', '$ip', '$phone', '$email', '$content', '".time()."')");
	$ajax['s'] =1;
	die($json->encode($ajax));
}

//生成验证码, 返回vvc id
if($act == 'vvc'){

	$key = ForceStringFrom('key');
	$code = ForceStringFrom('code');
	$decode = authcode($code, 'DECODE', $key);

	if($decode != md5(WEBSITE_KEY . $_CFG['KillRobotCode'])){
		$ajax['s'] = 0;
		die($json->encode($ajax)); //验证码过期
	}

	$ajax['s'] = createVVC();
	die($json->encode($ajax));
}

//获取验证码图片
if($act == 'get'){
	getVVC();
	die();
}

?>